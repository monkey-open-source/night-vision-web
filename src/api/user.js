import request from '@/utils/request'

//登录
export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

//获取用户信息
export function getInfo() {
  return request({
    url: '/user/getUserInfo',
    method: 'get'
  })
}


// 登出
export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

// 获取用户列表
export function getUserList() {
  return request({
    url: '/user/list',
    method: 'get'
  })
}

// 添加新用户
export function addNewUser(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data
  })
}


// 删除用户
export function delUser(id) {
  return request({
    url: `/user/delete?id=${id}`,
    method: 'delete',
  })
}

// 修改用户信息
export function editUser(data) {
  return request({
    url: '/user/edit',
    method: 'put',
    data
  })
}

// 获取用户安全问题
export function getProblemList() {
  return request({
    url: '/problem/list',
    method: 'get',
  })
}


//修改用户密码
export function editUserPassword(data) {
  return request({
    url: '/user/changePwd',
    method: 'post',
    data
  })
}


//校验安全问题，修改密码
export function checkAnswersEdit(data) {
  return request({
    url: '/user/checkAnswers',
    method: 'post',
    data
  })
}

