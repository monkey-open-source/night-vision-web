import request from '../utils/request'

/**
 * 获取 Rtsp 流地址
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiGetRtspUrl = () => {
  return request.get('/camera/getRtspStream')
}

/**
 * 放大
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetZoomSmall = () => {
  return request.get('/camera/zoomm')
}

/**
 * 缩小
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetZoomBig = () => {
  return request.get('/camera/zooma')
}

/**
 * 焦距-停止
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetZoomStop = () => {
  return request.get('/camera/zooms')
}


/**
 * 获取目标跟踪状态
 */
export const apiGetFollowingState = () => {
  return request.get('/camera/detectGet')
}



/**
 * 目标跟踪停止
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetFollowingStop = () => {
  return request.get('/camera/detectSet?level=0')
}

/**
 * 目标跟踪开始
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetFollowingStart = () => {
  return request.get('/camera/detectSet?level=1')
}

/**
 * 获取增亮等级
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiGetBrightening = () => {
  return request.get('/camera/getLevel')
}

/**
 * 获取凸显等级
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiGetProminent = () => {
  return request.get('/camera/resolutionGet')
}

/**
 * 设置增量等级
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetBrightening = (e) => {
  return request.get('/camera/setLevel?level=' + e)
}

/**
 * 设置凸显等级
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiSetProminent = (e) => {
  return request.get('/camera/resolutionSet?level=' + e)
}

/**
 * 截图
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiPrintScreen = () => {
  return request.get('/camera/takeSnapshot')
}

/**
 * 开始录制
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiRecordingStart = () => {
  return request.get('/camera/RecordCameraSaveMp4')
}

/**
 * 结束录制
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export const apiRecordingStop = (id) => {
  return request.get('/camera/stopRecordCameraSaveMp4?fileId=' + id)
}
