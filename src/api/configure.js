import request from '@/utils/request'


//本地配置保存
export function editConfigLocal(data) {
  return request({
    url: '/config/editConfigLocal',
    method: 'post',
    data
  })
}

//修改系统基本信息
export function editConfigSystemBasic(data) {
  return request({
    url: '/config/editConfigSystemBasic',
    method: 'post',
    data
  })
}


//修改系统设置时间
export function editConfigSystemTime(data) {
  return request({
    url: '/config/editConfigSystemTime',
    method: 'post',
    data
  })
}

//获取配置列表
export function getConfigList(type) {
  return request({
    url: `/config/list?type=${type}`,
    method: 'get',
  })
}


//获取配置列表
export function resetConfig() {
  return request({
    url: '/config/resDefault',
    method: 'get',
  })
}


//重启系统
export function restartSystem() {
  return request({
    url: '/serialPortSender/shutdownLinux',
    method: 'get',
  })
}


//修改tcpip信息
export function editNetworkTcpIp(data) {
  return request({
    url: '/config/editConfigNetworkTCPIP',
    method: 'post',
    data
  })
}

//修改端口信息
export function editNetworkPort(data) {
  return request({
    url: '/config/editConfigNetworkPort',
    method: 'post',
    data
  })
}

//获取自适应网卡信息
export function getNetWorkInfo() {
  return request({
    url: '/monitor/server/getNetWorkInfo',
    method: 'get',
  })
}

//获取其他网卡信息
export function getNetWorkInfos() {
  return request({
    url: '/monitor/server/getNetWorkInfos',
    method: 'get',
  })
}


//修改GB信息
export function editNetworkGB(data) {
  return request({
    url: '/config/editConfigNetworkGBT28181',
    method: 'post',
    data
  })
}


//修改图像设置信息
export function editConfigImage(data) {
  return request({
    url: '/config/editConfigImage',
    method: 'post',
    data
  })
}

//修改存储录像计划设置
export function editRecordingPlan(data) {
  return request({
    url: '/config/editConfigStorageRecordingPlan',
    method: 'post',
    data
  })
}

//修改存储录像计划设置
export function editScreenShot(data) {
  return request({
    url: '/config/editConfigStorageScreenshot',
    method: 'post',
    data
  })
}

//网络自动获取地址
export function autoDHCP() {
  return request({
    url: '/config/editConfigNetworkAutoDHCP',
    method: 'get',
  })
}


//测试ip是否可用
export function ipIsAble(ip) {
  return request({
    url: `/config/isReachable?ipAddr=${ip}`,
    method: 'get',
  })
}


//获取硬盘管理信息
export function getServerInfo() {
  return request({
    url: '/monitor/server',
    method: 'get',
  })
}


//获取服务器时间
export function getServerTime() {
  return request({
    url: '/config/getSysTime',
    method: 'get',
  })
}


//修改系统时间
export function setServerTime(time) {
  return request({
    url: `/config/setSysTime?newTimeStr=${time}`,
    method: 'get',
  })
}


//获取OSD信息
export function getOsdInfo() {
  return request({
    url: '/config/getOSD',
    method: 'get',
  })
}

//设置OSD信息
export function setOsdInfo(data) {
  return request({
    url: '/config/setOSD',
    method: 'post',
    data
  })
}


//清空文件
export function clearAllFiles() {
  return request({
    url: '/config/clearFiles',
    method: 'get',
  })
}


//获取gb28181信息
export function getGBInfo() {
  return request({
    url: '/config/getGB',
    method: 'get',
  })
}

//设置gb28181信息
export function setGBInfo(data) {
  return request({
    url: '/config/setGB',
    method: 'post',
    data
  })
}

//重启定时任务
export function reStartRegular() {
  return request({
    url: '/config/resumeQuartz',
    method: 'get',
  })
}

//查询那个时间段有视频
export function getTimeFiles() {
  return request({
    url: '/file/isFiles',
    method: 'get',
  })
}

//获取系统ipv4信息
export function getIpv4Info() {
  return request({
    url: '/config/getIpv4Info',
    method: 'get',
  })
}



//获取自适应状态
export function getIsAble() {
  return request({
    url: '/config/getIsDHCP',
    method: 'get',
  })
}
