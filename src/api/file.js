import request from '@/utils/request'

//获取文件列表
export function getFileList(data) {
  return request({
    url: `/file/list?fileType=${data.fileType}&startTime=${data.startTime}&endTime=${data.endTime}&fileSuffix=${data.fileSuffix}`,
    method: 'get',
  })
}

/**
 * 根据条件获取文件列表
 * @param fileType 文件类型： 0 录制文件，1 定时文件
 * @param startTime 开始时间
 * @param endTime 结束时间
 * @returns {*}
 */
export const apiFilter = (fileType, startTime, endTime) => {
  return request.get(`/file/list?fileType=${fileType}&startTime=${startTime}&endTime=${endTime}`)
}



//根据文件路径下载文件
export function fileDownloadByPath(path) {
  return request({
    url: `/file/download?fullPath=${path}`,
    method: 'get',
    responseType: 'blob',
  })
}

//根据id删除文件
export function deleteFileById(id) {
  return request({
    url: `/file/deleteById?id=${id}`,
    method: 'delete',
  })
}


//根据日期获取文件列表
export function getListByDate(date) {
  return request({
    url: `/file/getListByDate?dateString=${date}`,
    method: 'get',
  })
}


//插件下载
export function getPluginDown() {
  return request({
    url: '/file/downloadFile',
    method: 'get',
  })
}
