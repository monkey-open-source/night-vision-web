import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Router from './router/index.js'
import pinia from './store/index'
import '@/permission'
import 'element-plus/dist/index.css'
import '@/utils/jsmpeg.min'

const app = createApp(App)
app.use(Router)
app.use(pinia)
app.mount('#app')
