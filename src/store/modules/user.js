import { defineStore } from "pinia"
import { login, getInfo, logout } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'

export const useUserStore = defineStore("user", {
  state: () => {
    return {
      token: getToken() || '',
      username: '',
      userType: '',
      id: '',
      count: 0,
      playbackDownloadSavePath: "",
      playbackScreenshotSavePath: "",
      previewScreenshotSavePath: "",
      videoFileSavePath: "",
    }
  },
  getters: {
    // double: (state) => state.count * 2,
  },
  actions: {
    // increment() {
    //   this.count++
    // },
    userlogin(data) {
      return new Promise((resolve, reject) => {
        login({
          userName: data.username,
          password: data.password
        }).then((info) => {
          if (info.code === 200) {
            setToken(info.data.token);
            this.token = info.data.token;
          }
          resolve(info)
        }, (error) => {
          reject(error)
        })
      })


    },
    getUserInfo() {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          this.username = response.data.userName;
          this.id = response.data.id;
          this.userType = response.data.userType;

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    userLogout() {
      return new Promise((resolve, reject) => {
        logout().then(response => {
          removeToken()
          this.username = '';
          this.id = '';
          this.userType = '';
          this.token = '';
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    resetToken() {
      return new Promise(resolve => {
        removeToken()
        resolve()
      })
    },
  },
})