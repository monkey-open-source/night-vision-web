import { createRouter, createWebHistory } from 'vue-router'
import 'nprogress/nprogress.css'

const routerHistory = createWebHistory();

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/',
    name: 'Layout',
    component: () => import('../views/LayoutView.vue'),
    children: [
      {
        path: '/preview',
        name: 'Preview',
        component: () => import('../views/top-views/PreviewView.vue'),
        meta: {
          topCurrent: 0
        }
      },
      {
        path: '/playback',
        name: 'Playback',
        component: () => import('../views/top-views/PlaybackView.vue'),
        meta: {
          topCurrent: 1
        }
      },
      {
        path: '/image',
        name: 'Image',
        component: () => import('../views/top-views/ImageView.vue'),
        meta: {
          topCurrent: 2
        }
      },
      {
        path: '/allocation',
        name: 'Allocation',
        component: () => import('../views/top-views/AllocationView.vue'),
        meta: {
          topCurrent: 3
        },
        children: [
          {
            path: '/allocation/local',
            name: '',
            component: () => import('../views/top-views/allocation-views/LocalView.vue'),
            meta: {
              sideCurrent: 0
            }
          },
          {
            path: '/allocation/system',
            name: '',
            component: () => import('../views/top-views/allocation-views/SystemView.vue'),
            meta: {
              sideCurrent: 1
            }
          },
          {
            path: '/allocation/network',
            name: '',
            component: () => import('../views/top-views/allocation-views/NetworkView.vue'),
            meta: {
              sideCurrent: 2
            }
          },
          {
            path: '/allocation/image',
            name: '',
            component: () => import('../views/top-views/allocation-views/ImageView.vue'),
            meta: {
              sideCurrent: 3
            }
          },
          {
            path: '/allocation/oss',
            name: '',
            component: () => import('../views/top-views/allocation-views/OssView.vue'),
            meta: {
              sideCurrent: 4
            }
          }
        ]
      }
    ]
  }
];

const router = createRouter({
  history: routerHistory,
  routes,
});

// NProgress.configure({
//   showSpinner: false
// })

// router.beforeEach((to, from, next) => {
//   NProgress.start()
//   next()
// })

// router.afterEach(() => {
//   NProgress.done()
// })

export default router;
