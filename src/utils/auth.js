const TokenKey = 'vue_night_vision_token'

export function getToken() {
  // return localStorage.getItem(TokenKey)
  return sessionStorage.getItem(TokenKey)
}

export function setToken(token) {
  // return localStorage.setItem(TokenKey, token)
  return sessionStorage.setItem(TokenKey, token)
}

export function removeToken() {
  // return localStorage.removeItem(TokenKey)
  return sessionStorage.removeItem(TokenKey)
}
