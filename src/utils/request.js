import axios from 'axios'
import { useUserStore } from "@/store/modules/user.js";



const service = axios.create({
  baseURL: '/api',
  timeout: 0
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    const store = useUserStore();
    const token = store.token
    if (token) {
      // 将token放在请求头中进行携带
      config.headers['token'] = token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    return res
  },
  error => {



    return Promise.reject(error)
  }
)

export default service
